﻿namespace laba5
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.ArraySize = new System.Windows.Forms.NumericUpDown();
            this.ManualInput = new System.Windows.Forms.RadioButton();
            this.RandomInput = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.list = new System.Windows.Forms.DataGridView();
            this.button = new System.Windows.Forms.Button();
            this.info = new System.Windows.Forms.Label();
            this.infoa = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ArraySize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.list)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(45, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Размер массива";
            // 
            // ArraySize
            // 
            this.ArraySize.Location = new System.Drawing.Point(48, 63);
            this.ArraySize.Name = "ArraySize";
            this.ArraySize.Size = new System.Drawing.Size(90, 20);
            this.ArraySize.TabIndex = 1;
            // 
            // ManualInput
            // 
            this.ManualInput.AutoSize = true;
            this.ManualInput.Location = new System.Drawing.Point(257, 49);
            this.ManualInput.Name = "ManualInput";
            this.ManualInput.Size = new System.Drawing.Size(66, 17);
            this.ManualInput.TabIndex = 2;
            this.ManualInput.TabStop = true;
            this.ManualInput.Text = "вручную";
            this.ManualInput.UseVisualStyleBackColor = true;
            // 
            // RandomInput
            // 
            this.RandomInput.AutoSize = true;
            this.RandomInput.Location = new System.Drawing.Point(257, 66);
            this.RandomInput.Name = "RandomInput";
            this.RandomInput.Size = new System.Drawing.Size(71, 17);
            this.RandomInput.TabIndex = 3;
            this.RandomInput.TabStop = true;
            this.RandomInput.Text = "случайно";
            this.RandomInput.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(257, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Ввод массива";
            // 
            // list
            // 
            this.list.AllowUserToAddRows = false;
            this.list.AllowUserToDeleteRows = false;
            this.list.AllowUserToResizeColumns = false;
            this.list.AllowUserToResizeRows = false;
            this.list.BackgroundColor = System.Drawing.SystemColors.Menu;
            this.list.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.list.CausesValidation = false;
            this.list.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.list.Location = new System.Drawing.Point(48, 126);
            this.list.MultiSelect = false;
            this.list.Name = "list";
            this.list.ReadOnly = true;
            this.list.RowHeadersVisible = false;
            this.list.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.list.ShowEditingIcon = false;
            this.list.Size = new System.Drawing.Size(601, 50);
            this.list.TabIndex = 5;
            // 
            // button
            // 
            this.button.Location = new System.Drawing.Point(467, 46);
            this.button.Name = "button";
            this.button.Size = new System.Drawing.Size(182, 37);
            this.button.TabIndex = 6;
            this.button.Text = "Вычислить";
            this.button.UseVisualStyleBackColor = true;
            // 
            // info
            // 
            this.info.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.info.Location = new System.Drawing.Point(49, 179);
            this.info.Name = "info";
            this.info.Size = new System.Drawing.Size(600, 38);
            this.info.TabIndex = 7;
            this.info.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // infoa
            // 
            this.infoa.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.infoa.Location = new System.Drawing.Point(49, 217);
            this.infoa.Name = "infoa";
            this.infoa.Size = new System.Drawing.Size(600, 38);
            this.infoa.TabIndex = 8;
            this.infoa.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Menu;
            this.ClientSize = new System.Drawing.Size(697, 281);
            this.Controls.Add(this.infoa);
            this.Controls.Add(this.info);
            this.Controls.Add(this.button);
            this.Controls.Add(this.list);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.RandomInput);
            this.Controls.Add(this.ManualInput);
            this.Controls.Add(this.ArraySize);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.ArraySize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.list)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown ArraySize;
        private System.Windows.Forms.RadioButton ManualInput;
        private System.Windows.Forms.RadioButton RandomInput;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView list;
        private System.Windows.Forms.Button button;
        private System.Windows.Forms.Label info;
        private System.Windows.Forms.Label infoa;
    }
}

