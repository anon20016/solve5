﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace laba5
{
    /// <summary>
    /// Решение 5го задания
    /// </summary>
    public partial class Form1 : Form
    {
        Random rnd = new Random();
        /// <summary>
        /// Конструктор формы
        /// </summary>
        public Form1()
        {
            InitializeComponent();

            (Controls["ArraySize"] as NumericUpDown).Value = 15;       
            init(genArray((int)(Controls["ArraySize"] as NumericUpDown).Value));
            (Controls["RandomInput"] as RadioButton).Checked = true;
            (Controls["list"] as DataGridView).EditingControlShowing += Form1_EditingControlShowing;
            (Controls["ManualInput"] as RadioButton).CheckedChanged += ChangeManual;
            (Controls["RandomInput"] as RadioButton).CheckedChanged += ChangeRandom;
            (Controls["ArraySize"] as NumericUpDown).ValueChanged += Form1_ValueChanged;
            (Controls["button"] as Button).Click += Button_Click;

            
        }


        /// <summary>
        /// Валидация ввода цифр в таблице при редактировании
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            TextBox tb = (TextBox)e.Control;
            tb.KeyPress += new KeyPressEventHandler(tb_KeyPress);
        }

        /// <summary>
        /// Обработчик нажатия на кнопку решения задачи
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, EventArgs e)
        {
            Solve();
        }

        /// <summary>
        /// Изменение массива вручную (Radiobuton)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeManual(object sender, EventArgs e)
        {
            (Controls["list"] as DataGridView).ReadOnly = false;
        }
        /// <summary>
        /// Изменение массива случайно (Radiobuton)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangeRandom(object sender, EventArgs e)
        {
            if ((sender as RadioButton).Checked)
            {
                (Controls["list"] as DataGridView).ReadOnly = true;
                refresh();
            }
        }

        /// <summary>
        /// Обработчик изменения поля ввода количества элементов
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_ValueChanged(object sender, EventArgs e)
        {
            int new_count = (int)(Controls["ArraySize"] as NumericUpDown).Value;

            var r = (Controls["list"] as DataGridView).Rows;
            var c = (Controls["list"] as DataGridView).Columns;

            if (new_count > 0)
            {
                if (c.Count > 0)
                {
                    for (int i = 0; i < c.Count; i++)
                    {
                        c[i].Width = 600 / new_count;
                    }
                    for (int i = c.Count; i < new_count; i++)
                    {
                        c.Add((i + 1).ToString(), (i + 1).ToString());
                        r[0].Cells[i].Value = rnd.Next(200) - 100;
                    }

                    (Controls["list"] as DataGridView).Refresh();
                }
            }
        }

        /// <summary>
        /// Инициализация таблицы
        /// </summary>
        /// <param name="a"></param>
        void init(List<int> a)
        {
            var c = (Controls["list"] as DataGridView).Columns;
            for (int i = 1; i <= a.Count; i++)
            {
                c.Add(i.ToString(), i.ToString());
                c[c.Count - 1].Width = 600 / a.Count;
            }
            var r = (Controls["list"] as DataGridView).Rows;
            r.Add();
            for (int i = 0; i < a.Count; i++)
            {
                r[0].Cells[i].Value = a[i];
            }
        }

        /// <summary>
        /// Обновить таблицу (DataGridView)
        /// </summary>
        void refresh()
        {
            List<int> a = genArray((int)(Controls["ArraySize"] as NumericUpDown).Value);
            var r = (Controls["list"] as DataGridView).Rows;
            for (int i = 0; i < a.Count; i++)
            {
                r[0].Cells[i].Value = a[i];
            }

            (Controls["list"] as DataGridView).Refresh();
        }
       
        /// <summary>
        /// Генерация случайного массива заданного размера
        /// </summary>
        /// <param name="n">Размер массива</param>
        /// <returns></returns>
        List<int> genArray(int n)
        {
            List<int> result = new List<int>(n);
            for (int i = 0; i < n; i++)
            {
                result.Add(rnd.Next(200) - 100);
            }
            return result;
        }

        /// <summary>
        /// Обновить поля, где вывоится решение задания
        /// </summary>
        /// <param name="s">Первая строка</param>
        /// <param name="c">Вторая строка</param>
        void RefreshLabel(string s, string c)
        {
            Controls["info"].Text = s;
            Controls["infoa"].Text = c;
            Controls["info"].Refresh();
            Controls["infoa"].Refresh();
        }

        /// <summary>
        /// Решение задания
        /// </summary>
        void Solve()
        {
            int fr3 = -1;
            int lst3 = -1;
            var t = (Controls["list"] as DataGridView).Rows[0].Cells;
            int n = (int)(Controls["ArraySize"] as NumericUpDown).Value;
            for (int i = 0; i < n; i++)
            {
                if (getCeilValue(i) % 3 == 0)
                {
                    lst3 = i;
                    fr3 = (fr3 == -1) ? i : fr3;
                }
            }
            if (fr3 == -1 || lst3 == fr3)
            {
                RefreshLabel("Нет необходимых элементов", "");
            }
            else
            {
                int sum = 0;
                for (int i = fr3 + 1; i < lst3; i++)
                {
                    sum += getCeilValue(i);
                }
                StringBuilder sb = new StringBuilder();
                sb.Append("[");
                for (int i = 0; i < n; i++)
                {
                    if (i == fr3)
                    {
                        sb.Append(getCeilValue(lst3).ToString());
                        sb.Append(", ");
                        continue;
                    }
                    if (i == lst3)
                    {
                        sb.Append(getCeilValue(fr3).ToString());
                        sb.Append(", ");
                        continue;
                    }
                    sb.Append(getCeilValue(i).ToString());
                    sb.Append(", ");
                }
                sb.Append("]");

                RefreshLabel("Сумма " + sum.ToString(), sb.ToString());
            }
        }

        private int getCeilValue(int x)
        {
            int y = 0;
            if ((Controls["list"] as DataGridView).Rows[0].Cells[x].Value.ToString().Length == 0)
            {
                if ((Controls["list"] as DataGridView).Rows[0].Cells[x].EditedFormattedValue.ToString().Length == 0)
                {
                    throw new FormatException();
                }
            }
            if ((Controls["list"] as DataGridView).Rows[0].Cells[x].IsInEditMode)
                int.TryParse((Controls["list"] as DataGridView).Rows[0].Cells[x].EditedFormattedValue.ToString(), out y);
            else
                int.TryParse((Controls["list"] as DataGridView).Rows[0].Cells[x].Value.ToString(), out y);           
            return y;
        }

        void tb_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsNumber(e.KeyChar)){
                if ((e.KeyChar != (char)Keys.Back) && (e.KeyChar != (char)Keys.Delete))
                {
                    e.Handled = true;
                }
            }
        }
    }

}



